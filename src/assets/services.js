export const ABOUT = [
    {
      id: 0,
      name: "M.Sc @ RWTH Counseling",
      image: "assets/images/uthappizza.png",
      category: "mains",
      label: "Hot",
      price: "",
      link:"Counseling",
      description:
        "A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer."
    },
    {
      id: 1,
      name: "Certificate Courses @ RWTH",
      image: "assets/images/zucchipakoda.png",
      category: "appetizer",
      label: "",
      price: "",
      link:"M.Sc @ RWTH Counseling",
      description:
        "Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce"
    },
    {
      id: 2,
      name: "Summer & Winter School",
      image: "assets/images/vadonut.png",
      category: "appetizer",
      label: "",
      price: "",
      link:"M.Sc @ RWTH Counseling",
      description:
        "A quintessential ConFusion experience, is it a vada or is it a donut?"
    },
    {
      id: 3,
      name: "M.Sc @ German Universities",
      image: "assets/images/vadonut.png",
      category: "appetizer",
      label: "",
      price: "",
      link:"germanycounseling",
      description:
        "A quintessential ConFusion experience, is it a vada or is it a donut?"
    },
    {
      id: 4,
      name: "Masters/PhD thesis",
      image: "assets/images/vadonut.png",
      category: "appetizer",
      label: "",
      price: "",
      link:"M.Sc @ RWTH Counseling",
      description:
        "A quintessential ConFusion experience, is it a vada or is it a donut?"
    },
    {
      id: 5,
      name: "Journal Publication",
      image: "assets/images/vadonut.png",
      category: "appetizer",
      label: "",
      price: "",
      link:"M.Sc @ RWTH Counseling",
      description:
        "A quintessential ConFusion experience, is it a vada or is it a donut?"
    },
    {
      id: 6,
      name: "Scholarships",
      image: "assets/images/vadonut.png",
      category: "appetizer",
      label: "",
      price: "",
      link:"Scholarships",
      description:
        "A quintessential ConFusion experience, is it a vada or is it a donut?"
    }
       
  ]