export const CONTENT = [
    {
        id: 0,
        heading: "Why choose Germany?",
        image: "../../Germany.jpg",
        
        description:
          " Germany is  the land of innovation and creativity, it is the 3rd most popular destination for international students, with its beautiful landscapes and exciting cities. It is one of the safest country in the world and houses great cultural diversity. German university degrees are highly respected by employers worldwide and it is one of the the primary locations for researching and developing new technologies in mechanical engineering."
      }
]