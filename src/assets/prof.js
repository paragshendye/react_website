export const TEAM = [
    {
      id: "Sandeep",
      name: "Dr. Sandeep Patil",
      image: "../../sandeep.jpg",
      Designation: "Managing Director",
      side:"left",
      linkedin:"http://www.linkedin.com/in/sandeepppatil",
      orcid:"https://orcid.org/0000-0003-3980-6995",
      scholar:"http://scholar.google.de/citations?user=5Qf-i9MAAAAJ&hl=en",
      rg:"https://www.researchgate.net/profile/Sandeep_Patil35",
      linkscholar:"ai ai-google-scholar-square ai-9x",
      linkrg:"ai ai-researchgate-square ai-7x",
      linkorcid:"ai ai-orcid-square ai-7x",
      linklinkedin:"fa fa-linkedin fa-7x",
      
      
      description:
        "Dr. Sandeep P. Patil is a Guest Scientist at the Institute of General Mechanics, RWTH Aachen University, Germany. In 2010, he finished his Masters of Science in Computational Mechanics of Materials and Structures, University of Stuttgart, Germany. He obtained his PhD (Dr.-Ing.) under the supervision of Professor Bernd Markert at the Institute of General Mechanics at the RWTH Aachen University, Germany, in 2015. He was a Scientific Assistant and a team leader at RWTH Aachen University, Germany, from Jan. 2015 to Oct. 2020. He is an Academic Study Advisor for M.Sc. Degree Programs (CAME and MME-CAME) at the RWTH Aachen University. His fields of interest are Continuum mechanics, Molecular dynamics simulation, Multiscale modeling, Advanced materials, Brittle fracture, Material forming and Biomaterials. He has authored above 20 peer-reviewed journal publications and more than 11 conference proceedings. He is a recipient of a Hilti Scholarship 2010. Hilti Corporation offered a scholarship to the master thesis at Schaan, Principality of Liechtenstein."
    },
    {
      id: "Bernd",
      name: "Prof. Bernd Markert",
      image: "../../Bernd_Markert.jpg",
      Designation: "Scientific Advisor",
      side:"right",
      linkedin:"https://www.linkedin.com/in/bernd-markert-a230a764/?originalSubdomain=de",
      orcid:"https://orcid.org/0000-0001-7893-6229",
      scholar:"https://scholar.google.com/citations?user=fychvLYAAAAJ&hl=en&oi=sra",
      rg:"https://www.researchgate.net/profile/Bernd_Markert",
      linkscholar:"ai ai-google-scholar-square ai-9x",
      linkrg:"ai ai-researchgate-square ai-7x",
      linkorcid:"ai ai-orcid-square ai-7x",
      linklinkedin:"fa fa-linkedin fa-7x",
      
      
      
      description:
        "Prof. Bernd Markert holds a diploma degree (Dipl.-Ing.) in Civil Engineering from the University of Stuttgart. He finished his PhD (Dr.-Ing.) at the Institute of Applied Mechanics (CE) at the University of Stuttgart in 2005. In 2013, he became a supernumerary professor (apl. Prof.) at the University of Stuttgart. In August 2013, Bernd Markert was appointed full professor at the RWTH Aachen University and Director of the Institute of General Mechanics in the Faculty of Mechanical Engineering. His present research focuses on the theory and numerics of general coupled problems, computational and experimental biomechanics and structural mechanics, as well as extended continuum methods, such as phase-field models."
      },
      {
        id: "Helmut",
        name: "Dr. Helmut Dinger",
        image: "../../DrDinger.jpg",
        Designation: "Business Advisor",
        side:"left",
        
        description:
          "Dr. Helmut Dinger has been Managing Director of the RWTH International Academy GmbH since 2010 and additionally assumed the role of Managing Director of the RWTH Business School in 2016. He graduated in Business Administration from Leopold Franzens University Innsbruck in 1995. In 1998, he received his Dr. oec. degree from the University of St. Gallen with a thesis on target costing in product development projects. He then worked as a consultant for various companies in the fields of innovation and technology. Since 1998, Dr. Dinger has regularly offered training and coaching in the areas of “Strategic Management, Entrepreneurship and Controlling” for various major European corporations and educational institutions such as Allianz, Alstom, Schweizer Fernsehen, Executive MBA-HSG or Management Circle."
      },
    
  ]