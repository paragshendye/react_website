import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import $ from 'jquery';
import Popper from 'popper.js';
import { NavLink } from 'reactstrap';
import { Link, animateScroll as scroll } from "react-scroll";



function NavBar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light" style={{"backgroundColor":"transparent"}}>
        {/* <a className="navbar-brand" href="#" style={{"color":"#006baf"}}>IAM</a> */}
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link text-uppercase ml-2" style={{"color":"#006baf"}} href="/" >Home <span className="sr-only">(current)</span></a>
            </li>
            <li className="nav-item">
            <a className="nav-link dropdown-toggle text-uppercase ml-2" href="#" style={{"color":"#006baf"}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                About Us
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item">About Us</a>
                <a className="dropdown-item" href="/IAM">IAM RWTH Aachen</a>
              
                <a className="dropdown-item" href="/RWTHAcademy" target="_blank">RWTH International Academy</a>
                <a className="dropdown-item" href="/RWTHAachenUniversity" target="_blank">RWTH Aachen University</a>
              </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-uppercase ml-2" style={{"color":"#006baf"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                RWTH Services
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="/Counseling">M.Sc. Counseling</a>
                <a className="dropdown-item" href="#">Certificate Courses</a>
                <a className="dropdown-item" href="#">Conferences</a>
                <a className="dropdown-item" href="#">Summer & Winter School</a>
                <a className="dropdown-item" href="#">Seminars</a>
                
              </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle  text-uppercase ml-2" style={{"color":"#006baf"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                German Universities
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="/WhyGermany">Why Germany</a>
                
                <a className="dropdown-item" href="/germanycounseling">M.Sc. Counseling</a>
                
              </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle  text-uppercase ml-2" style={{"color":"#006baf"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Research Counseling
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#">Masters/PhD Thesis</a>
                <a className="dropdown-item" href="/JournalPublication">International Journal Publication</a>
                <a className="dropdown-item" href="#">Projects</a>
                
              </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-uppercase ml-2" style={{"color":"#006baf"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Scholarships
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="/Scholarships">Internship @ RWTH Aachen</a>
                <a className="dropdown-item" href="#">Bachelor Thesis</a>
                <a className="dropdown-item" href="#">Master Thesis</a>
                <a className="dropdown-item" href="#">PhD & Research</a>
                <a className="dropdown-item" href="#">M.Sc Scholarships</a>
              </div>
            </li>
          </ul>
          {/* <form className="form-inline my-2 my-lg-0">
            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form> */}
        </div>
      </nav>
    )
}

export default NavBar;
