import React, { Component } from "react";
//import "react-responsive-carousel/lib/styles/carousel.css";
//import { Carousel } from 'react-responsive-carousel';
import "../style/Demo.css";
import { Carousel } from "react-bootstrap";

class DemoCarousel extends Component {

  render() {

    return (
      <div className="wrapper" style={{"textAlign":"center"}}>
        {/* <Carousel  autoplay showThumbs={false} showStatus={false}>
                <div>
                    <img src="PIC01.png" alt="img1" style={{"-WebkitTransform":"translateZ(0)","transform":"translateZ(0)"}}/>
                    <p className="legend">RWTH</p>
                </div>
                <div>
                    <img src="PIC02.png" alt="img2"/>
                    <p className="legend">RWTH</p>
                </div>
                <div>
                    <img src="PIC03.png" alt="img3" />
                    <p className="legend">Lecture</p>
                </div>
                <div>
                    <img src="PIC04.png" alt="img4" />
                    <p className="legend">Lecture</p>
                </div>
                <div>
                    <img src="PIC05.png"  alt="img5"/>
                    <p className="legend">Lecture</p>
                </div>
            </Carousel> */}
            
        <Carousel>
          <Carousel.Item >
            <img style={{"height":"70vh","width":"68vw"}}
              className="d-inline-block "
              src="../../PIC01.png"
              alt="First slide"
            />
            <Carousel.Caption>
              <h3>Convocation</h3>
            
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item >
            <img style={{"height":"70vh","width":"68vw"}}
              className="d-inline-block "
              src="../../PIC02.png"
              alt="Second slide"
            />
            <Carousel.Caption>
              <h3>Lecture</h3>
            
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item >
            <img style={{"height":"70vh","width":"68vw"}}
              className="d-inline-block "
              src="../../PIC03.png"
              alt="Third slide"
            />
            <Carousel.Caption>
              <h3>Lecture</h3>
            
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item >
            <img style={{"height":"70vh","width":"68vw"}}
              className="d-inline-block "
              src="../../PIC04.png"
              alt="Fourth slide"
            />
            <Carousel.Caption>
              <h3>IAM</h3>
            
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item >
            <img style={{"height":"70vh","width":"68vw"}}
              className="d-inline-block "
              src="../../PIC05.png"
              alt="Fifth slide"
            />
            <Carousel.Caption>
              <h3>IAM</h3>
            
            </Carousel.Caption>
          </Carousel.Item>
       
        </Carousel>
        
      </div>
    );
  }
}

export default DemoCarousel;
