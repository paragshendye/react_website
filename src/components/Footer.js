import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import styled from 'styled-components';
import {NavLink} from 'react-router-dom';

function Footer() {
  return (
    <div className="main-footer" style={{"background":"#006baf","padding":"1rem","color":"white","align-Content":"center","fontSize":"10px"}}>
        <div className="footer-middle" >
      <div className="container">
        <div className="row"> 
          {/* Column 1 */}
          <div className="col-sm-2 col-6" >
            <h6>Contact Us</h6>
            <ul className="list-unstyled">
              <li>IAM International Academy</li>
              <li>30, Campus Boulevard</li>
              <li>99999999</li>
              <li>xyz@rwth-aachen.de</li>
            </ul>
          </div>
          <div className="col-sm-2 col-6" >
            <h6>Other Details</h6>
            <ul className="list-unstyled">
              <li><a style={{"color":"darkgrey"}} href="/">Home</a></li>
              {/* <li><a style={{"color":"grey"}} href="/About Us">About Us</a></li> */}
              
              <li><a style={{"color":"darkgrey"}} href="/">About Us</a></li>
              
              <li><a style={{"color":"darkgrey"}} href="/">RWTH Services</a></li>
              <li><a style={{"color":"darkgrey"}} href="/">German Universities</a></li>
            </ul>
          </div>
          <div className="col-sm-2 col-6" style={{"paddingTop":"25px"}}>
            
            <ul className="list-unstyled">
              <li><a style={{"color":"darkgrey"}} href="/Counseling">M.Sc. Counseling</a></li>
              {/* <li><a style={{"color":"grey"}} href="/About Us">About Us</a></li> */}
              
              <li><a style={{"color":"darkgrey"}} href="/">Masters/PhD Thesis</a></li>
              
              <li><a style={{"color":"darkgrey"}} href="/">Certificate Courses</a></li>
              <li><a style={{"color":"darkgrey"}} href="/">Projects</a></li>
            </ul>
          </div>
          <div className="col-sm-2 col-6" style={{"paddingTop":"25px"}}>
            
            <ul className="list-unstyled">
              <li><a style={{"color":"darkgrey"}} href="/">Our Team</a></li>
              {/* <li><a style={{"color":"grey"}} href="/About Us">About Us</a></li> */}
              
              <li><a style={{"color":"darkgrey"}} href="/IAM">IAM RWTH Aachen</a></li>
              
              <li><a style={{"color":"darkgrey"}} href="/RWTHAachenUniversity">RWTH Aachen University</a></li>
              <li><a style={{"color":"darkgrey"}} href="/>RWTHAcademy">RWTH International Academy</a></li>
            </ul>
          </div>
          
          </div>
          
        </div>
      </div>
      <div className="footer-bottom">
            
      <p className="text-xs-left">
                &copy;{new Date().getFullYear()} 
                <p>Parag Shendye</p>
                 
              </p>
      </div>
    </div>
  );
}

export default Footer;

const FooterContainer = styled.footer` .footer-middle{
    background:var(--mainDark);
}`