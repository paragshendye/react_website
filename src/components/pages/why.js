import React, { Component } from 'react';
import {CONTENT} from '../../assets/content';
import NavBar from '../NavBar';
import Footer from '../Footer';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { content:CONTENT};
        
      }
     
      render() {
          
                const content = this.state.content.map(d => {
          return (
            
      
            
            <div key={d.id} className="col-md-12 col-12" >
                <h4>{d.heading}</h4>
                <img src={d.image} style={{"width":"70vw","height":"35vw"}}></img>
                <p>{d.description}</p>
              
            </div>
          );
        });
        return (
            <div>
            
    

        <div className="row">{content}</div>

        
        </div>

        );
      }  
    
}

 
export default Home;
