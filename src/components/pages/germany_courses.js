import React, { Component } from 'react';

class GermanyCounseling extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <h4>German Higher Education Institutions</h4>
                <p style={{"textAlign":"left"}}>
                    <ul>
                        <ol>Research Universities (Universität) carry out original academic work (i.e., research) in various subjects</ol>
                        <ol>Technical Universities (Technische Universität) traditionally specialize in science, technology and engineering research</ol>
                        <ol>Universities of Applied Science (Fachhochschulen) focus on practical subjects such as engineering, business or social science</ol>
                    </ul>
                </p>
                <h4>Outstanding research institutes outside universities</h4>
                <p style={{"textAlign":"left"}}>
                    <ul>
                        <ol>Max Planck Institutes</ol>
                        <ol>Helmholtz Research Centers</ol>
                        <ol>Fraunhofer Institutes</ol>
                    </ul>
                    
                </p>
                <h4>TU9 is the alliance of nine leading Technical Universities in Germany</h4>
                <p style={{"textAlign":"left"}}>
                    <ul>
                        <ol>RWTH Aachen University</ol>
                        <ol>Technical University of Berlin</ol>
                        <ol>Technical University of Braunschweig</ol>
                        <ol>Technical University of Darmstadt</ol>
                        <ol>Technical University of Dresden</ol>
                        <ol>Leibniz University Hannover</ol>
                        <ol>Karlsruhe Institute of Technology</ol>
                        <ol>Technical University of Munich</ol>
                        <ol>University of Stuttgart</ol>

                    </ul>
                </p>
            </div>
         );
    }
}
 
export default GermanyCounseling;