import React, { Component } from 'react';
import Footer from '../Footer';

class University extends Component {
  state = { images:[{id:0,source:"dfg1.jpg",link:"https://www.dfg.de/en/dfg_profile/index.html"},
    {id:1,source:"QS.jpg",link:"https://www.topuniversities.com/university-rankings/university-subject-rankings/2018/engineering-mechanical"},
    {id:2,source:"THE.jpg",link:"https://www.wiwo.de/erfolg/hochschule/personaler-ranking-das-sind-deutschlands-beste-unis/20832460.html"},
    {id:3,source:"wirts.jpg",link:"https://www.timeshighereducation.com/world-university-rankings/2018/subject-ranking/engineering-and-IT#!/page/0/length/25/sort_by/rank/sort_order/asc/cols/stats"}] }
  
 
    render() { 
    const images = this.state.images.map(d=>{
    return (
      
        
      
     
       <div id={d.id} className="col-md-3 col-sm-12 col-6" style={{"padding":"4px"}}> 
       <a href={d.link} target="_blank">
        <img src={d.source} style={{"height":"24vh"}}/>
        </a>
        </div>
        
       

     );
      });
    return(
      <div>
        <div><img src="../../RWTH_haupt.jpg" style={{"width":"70vw","height":"35vw"}}/></div>
        <div style={{"textAlign":"left","maxWidth":"100%"}}>
      <p>RWTH Aachen University is one of <a href="https://www.dfg.de/en/research_funding/programmes/excellence_initiative/index.html">Germany's Universities of Excellence</a>. It is a place where the future of our industrialized world is thought out. The University is proving to be a hotspot with increasing international recognition where innovative answers to global challenges are developed.</p>
      
      <ul style={{"textAlign":"left"}}>
        <ol>RWTH ranked 1st place in approved funding for Engineering 2018</ol>
        <ol>RWTH ranked 23rd place by subject for Engineering - Mechanical, Aeronautical & Manufacturing 2018</ol>
        <ol>RWTH ranked 1st place in Mechanical Engineering Electrical Engineering and Business Engineering 2018</ol>
        <ol>RWTH ranked 24th in Technology and Engineering 2018</ol>
        <ol>45,256 students; 23% of international students at RWTH: a hugely diverse student body!</ol>
        <ol>Over 260 research institutes</ol>
        <ol>the most third-party research funds in engineering in Germany</ol>
        <ol>Strong links to industry</ol>
      </ul>
      </div>
     
      <div className="row" style={{"paddingLeft":"50px"}}>{images}</div>
      </div>
    ) 
    }
 
}


 
export default University;