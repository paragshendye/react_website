import React, { Component } from "react";
import Footer from "../Footer";
 
class IamRwth extends Component {
  state = {};
  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <h2>IAM RWTH</h2>
        <div className="col-12 col-sm-12">
          <p>
            The Institute of General Mechanics has existed since the founding of
            the RWTH Aachen University in 1870. The institute was headed by
            eminent scientists, such as August Ritter, Arnold Sommerfeld,
            Theodor von Kármán and Wilhelm Müller. In August 2013, Bernd Markert
            was appointed full professor at the RWTH Aachen University and
            Director of the Institute of General Mechanics in the Faculty of
            Mechanical Engineering. His present research focuses on the theory
            and numerics of general coupled problems, computational and
            experimental biomechanics and structural mechanics, as well as
            extended continuum methods, such as phase-field models.
          </p>

          <p>
            The institute mainly focused on Statics, Resistance of Materials,
            Computational Intelligence, Artificial Neural Network, Molecular
            Mechanics, Multiscale Modeling, Dynamics, Structural Mechanics,
            Biomechanics, Smart Structures, Composites.
          </p>
        </div>
        <div style={{"textAlign":"left","paddingLeft":"20px"}}>
            <h4>Research @IAM RWTH</h4>
            <ul>
                <li><h6>Theoretical Modeling</h6>
                    <ul>
                        <ol>Advanced and extended continuum methods</ol>
                        <ol>Multiphase and porous media theories</ol>
                        <ol>Soft and hard tissue biomechanics</ol>
                        <ol>Nonlinear structural mechanics</ol>
                        <ol>Limit and shakedown analysis</ol>
                    </ul>
                </li>
            </ul>
            <ul>
                <li><h6>Theoretical Modeling</h6>
                    <ul>
                        <ol>Advanced and extended continuum methods</ol>
                        <ol>Multiphase and porous media theories</ol>
                        <ol>Soft and hard tissue biomechanics</ol>
                        <ol>Nonlinear structural mechanics</ol>
                        <ol>Limit and shakedown analysis</ol>
                    </ul>
                </li>
            </ul>
            <ul>
                <li><h6>Theoretical Modeling</h6>
                    <ul>
                        <ol>Advanced and extended continuum methods</ol>
                        <ol>Multiphase and porous media theories</ol>
                        <ol>Soft and hard tissue biomechanics</ol>
                        <ol>Nonlinear structural mechanics</ol>
                        <ol>Limit and shakedown analysis</ol>
                    </ul>
                </li>
            </ul>
        </div>
        
      </div>
    );
  }
}

export default IamRwth;
