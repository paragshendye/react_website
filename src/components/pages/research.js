import React, { Component } from "react";

class Research extends Component {
  state = {};
  render() {
    return (
      <div>
        <h4>International Journal Publications</h4>
        <ul style={{textAlign:"left"}}>
          <ol>
            <b>Sandeep P. Patil</b>, Parag Shendye, and Bernd Markert, 2020, Mechanical
            properties and behavior of glass fiber-reinforced silica aerogel
            nanocomposites: Insights from all-atom simulations Scr. Mater., 177,
            65-68
          </ol>
          <ol>
          Sandeep P. Patil, 2019, Nanoindentation of Graphene-Reinforced Silica Aerogel: A
Molecular Dynamics Study, Molecules, 24, 1336
          </ol>
          <ol>
          Sandeep P. Patil, Parag Shendye, and Bernd Markert, 2019, Molecular dynamics
investigation of the shock response of silica aerogels, Materialia, 6, 100315
          </ol>
          <ol>
          Sandeep P. Patil, Vinayak G. Parale, Hyung-Ho Park, and Bernd Markert, 2019,
Molecular dynamics and experimental studies of nanoindentation on nanoporous silica
aerogels, Mater. Sci. Eng., A, 742, 344–352
          </ol>
          <ol>
          Sandeep P. Patil, Vinayak G. Parale, Hyung-Ho Park, and Bernd Markert, 2019,
Molecular dynamics and experimental studies of nanoindentation on nanoporous silica
aerogels, Mater. Sci. Eng., A, 742, 344–352
          </ol>
          <ol>
          Sandeep P. Patil, Ameya Rege, Mikhail Itskov, and Bernd Markert, 2019, Fracture
of silica aerogels: An all-atom simulation study, J. Non-Cryst. Solids, 498, 125–129

          </ol>
          <ol>
          Sandeep P. Patil, Sri Harsha Chilakamarri, and Bernd Markert, 2018, A novel
nonlinear nano-scale wear law for metallic brake pads, Phys. Chem. Chem. Phys.,
20(17), 12027–12036
          </ol>
          <ol>
          Sandeep P. Patil, Ameya Rege, Sagardas, Mikhail Itskov, and Bernd Markert,
2017, Mechanics of Nanostructured Porous Silica Aerogel Resulting From Molecular
Dynamics Simulations, J. Phys. Chem. B, 121(22), 5660–5668
          </ol>
          <ol>
          Johannes A. Wagner, Sandeep P. Patil, Imke Greving, Marc Lämmel, Konstantinos
Gkagkas, Tilo Seydel, Martin Müller, Bernd Markert, and Frauke Gräter, 2017,
Stress-induced long-range ordering in spider silk, Sci. Rep., 7(1), 15273
          </ol>
          <ol>
          Sandeep P. Patil, Yousef Heider, Carlos A. Hernandez Padilla, Eduardo R. Cruz-Chú,
and Bernd Markert, 2016, A comparative molecular dynamics-phase-field modeling
approach to brittle fracture, Comput. Methods in Appl. Mech. Eng., 312, 117–129
          </ol>
          <ol>
          Sandeep P. Patil, Bernd Markert, and Frauke Gräter, 2014, Rate-dependent behavior
of the amorphous phase of spider dragline silk, Biophys. J., 106(11), 2511–2518
          </ol>

        </ul>
        
        <ul style={{"textAlign":"left"}}>
        <ol><h6>For more detail, please refer to the following profile links</h6></ol>
            <ol><a href="http://scholar.google.de/citations?user=5Qf-i9MAAAAJ&hl=en" target="_blank">Google Scholar</a></ol>
            <ol><a href="https://orcid.org/0000-0003-3980-6995" target="_blank">ORCID</a></ol>
            <ol><a href="http://www.linkedin.com/in/sandeepppatil" target="_blank">LinkedIn</a></ol>
            <ol><a href="https://www.researchgate.net/profile/Sandeep_Patil35" target="_blank">ResearchGate</a></ol>
        </ul>

      </div>
    );
  }
}

export default Research;
