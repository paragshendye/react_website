import React, { Component } from "react";
import "../../style/team.css";
//import ReactCardFlip from 'react-card-flip';
import { TEAM } from "../../assets/prof";
import ReadMoreReact from "read-more-react";



import {
  Card,
  CardImg,
  CardSubtitle,
  Button,
  CardImgOverlay,
  CardText,
  CardBody,
  CardTitle
} from "reactstrap";

class Team extends Component {
  constructor(props) {
    super(props);
    this.state = { team: TEAM };
  }

  render() {
    const professor = this.state.team.map(d => {
      return (
        //   <div className={styles['card-container']}>
        //  <UserCard
        //   header="../../back_prof.jpg"
        //   avatar="../../1.jpg"
        //   name={d.name}
        //   positionName={d.Education}

        //  />
        //  </div>
        <div key={d.id} className="col-md-12 col-sm-12 col-xs-12">
          {/* <div className="cont">
          <div class="card" style={{marginTop:"10px"}}>
            <img
              className="team_img"
              src={d.image}
              alt="Jane"
              style={{ width: "50%","alignSelf":"center" }}
            />
            <h4 style={{ color: "white" }}>{d.name}</h4>
            <p className="title" style={{color:"white"}, {"fontSize":"20px"}}>{d.Designation}</p>
            
            <ReadMoreReact text={d.description} max={100} readMoreText="more..."/>
            
            
            
          </div>
          </div> */}
          
          <div style={{ textAlign: d.side ,"paddingTop":"5px"}}>
            <img src={d.image} alt={d.name} />
          </div>
          <div style={{ textAlign: d.side }}>
            <div id={d.id}>
             <h4 style={{"color":"black"}}>{d.name}</h4>
            <h6 style={{ color: "black" }}>{d.Designation}</h6>
            </div>
            <div>
            <div>
            <a href={d.scholar} target="_blank"><i style={{fontSize:"30px"}} className={d.linkscholar}></i></a>
            <a style={{"paddingLeft":"5px"}} href={d.rg} target="_blank"><i style={{"color":"#25D9A4",fontSize:"30px"}}className={d.linkrg}></i></a>
            <a style={{"paddingLeft":"10px"}} href={d.linkedin} target="_blank"><i style={{fontSize:"30px"}} className={d.linklinkedin}></i></a>
            <a style={{"paddingLeft":"10px"}} href={d.orcid} target="_blank"><i style={{"color":"#98F10D",fontSize:"30px"}} className={d.linkorcid}></i></a>
            
            </div>
            <p style={{ color: "#95A5A6" }}>{d.description}</p>
            
            <hr className="dash" />
            </div>
          </div>
        </div>
      );
    });
    return (
      <div className="row" id="Team">
        {professor}
      </div>
    );
  }
}

export default Team;
