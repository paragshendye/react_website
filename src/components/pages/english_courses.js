import React, { Component } from 'react';


class EnglishCourses extends Component {
  state = {  }
  render() { 
    return (
     <div style={{"textAlign":"center"}}> 

      <h2>English-Taught Master's Programs at RWTH Aachen University</h2>
      <div className="col-12 col-sm-12">
      <h4>Master‘s Degree Programs in Mechanical Engineering</h4>
      <ul style={{"textAlign":"left","paddingTop":"20px"}}>
        <ol>M.Sc. Computer Aided Conception and Production in Mechanical Engineering (CAME)</ol>
        <ol>M.Sc. Networked Production Engineering (NPE)</ol>
        <ol>M.Sc. Robotic Systems Engineering (RoboSys)</ol>
        <ol>M.Sc. Textile Engineering (Textile)</ol>
        <ol>M.Sc. Production Systems Engineering (PSE)</ol>
       
      </ul>
      </div>
      <div className="col-12 col-sm-12">
      <h6 style={{"textAlign":"left","paddingTop":"20px"}}>Key Facts</h6>
      <ul style={{"textAlign":"left","paddingTop":"20px"}}>
        <ol>Master of Science awarded by RWTH Aachen University</ol>
        <ol>Accreditation by ASIIN e.V.</ol>
        <ol>English-taught program</ol>
        <ol>120 CP  | 2-year duration</ol>
        <ol>Fees: EUR 4,500 per semester</ol>
        <ol>Start: Every year in October |  Application Deadline: 1 March</ol>
       
      </ul>
      </div>
      <div className="col-12 col-sm-12">
      <h4>Master‘s Degree Programs in Management and Engineering</h4>
      <ul style={{"textAlign":"left","paddingTop":"20px"}}>
        <ol>M.Sc. Management and Engineering in Computer Aided Mechanical Engineering (MME-CAME)</ol>
        <ol>M.Sc. Management and Engineering in Production Systems (MME-PS)</ol>
        <ol>M.Sc. Management and Engineering in Structural Engineering and Risk Management of Industrial Facilities (MME-CONSTRUCT)</ol>
        <ol>M.Sc. Management and Engineering in Technology, Innovation, Marketing and Entrepreneurship (MME TIME)</ol>
        <ol>M.Sc. Data Analytics and Decision Science (DDS)</ol>
       
      </ul>
      </div>
      <div className="col-12 col-sm-12">
      <h6 style={{"textAlign":"left","paddingTop":"20px"}}>Key Facts</h6>
      <ul style={{"textAlign":"left","paddingTop":"20px"}}>
        <ol>Master of Science awarded by RWTH Aachen University</ol>
        <ol>Accreditation by ASIIN e.V.</ol>
        <ol>English-taught program</ol>
        <ol>120 CP  | 2-year duration</ol>
        <ol>Fees: EUR 5,500 per semester</ol>
        <ol>Start: Every year in October |  Application Deadline: 1 March</ol>
       
      </ul>
      </div>
      
      </div>
       

     );
  }
}
 
export default EnglishCourses;