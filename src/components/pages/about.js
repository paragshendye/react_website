import React, { Component } from "react";
import { ABOUT } from "../../assets/services";
import "../../style/about.css";
import ReadMoreReact from 'read-more-react';
import {
  Card,
  CardImg,
  CardSubtitle,
  Button,
  CardImgOverlay,
  CardText,
  CardBody,
  CardTitle,
  CardLink
} from "reactstrap";


class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      about: ABOUT
    };
  }
 
  render() {
    const services = this.state.about.map(d => {
      var link = "/"+d.link
      return (
        <div key={d.id} className="col-md-4 col-sm-12 col-xs-12" style={{"paddingLeft":"20px","paddingRight":"20px","paddingTop":"10px","paddingBottom":"10px"}}>
        <a href={link}>  <Card className="aboutcard" style={{"height":"35vh", "width":"100%"}}>
            <CardBody style={{"backgroundColor":"#006baf"}}>
      <CardTitle  style={{"color":"white","fontSize":"18px"}}>{d.name}</CardTitle>

              
      {/* <p>{d.description}</p> */}
            </CardBody>
            
            <CardImg
              bottom
              width="100%"
              src="../../1.jpg"
               alt="Card image cap"
            />

     
          </Card></a>
        </div>
      );
    });
    return <div className="row">{services}</div>;
  }
}

export default About;
