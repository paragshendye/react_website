import React, { Component } from "react";

class Scholarships extends Component {
  state = {};
  render() {
    return (
      <div>
        <h4>
          DAAD RISE (Research Internships in Science and Engineering)
          scholarship @RWTH
        </h4>
        <p style={{ textAlign: "left" }}>
          <ul>
            <ol>8-12 weeks in summer</ol>
            <ol>Web-based matching process</ol>
            <ol>
              Undergrads work directly with German doctoral students at top labs
              across the country
            </ol>
            <ol>No German required</ol>
            <ol>Scholarships and housing assistance provided</ol>
            <ol>More than 500 interns placed in 2018</ol>
            <ol>Intensive Language Courses in Germany</ol>
            <ol>
              <a href="https://www.daad.org/ISK_undergrad" target="_blank">
                click here to apply
              </a>
            </ol>
          </ul>
        </p>
        <h4>
          Eight-week intensive courses at leading language institutes in Germany
        </h4>
        <p style={{ textAlign: "left" }}>
          <h6 style={{marginLeft:"25px"}}>Requirements</h6>
          <ul>
            <ol>
              undergraduates or graduate students in any field of study except
              German Studies, German Language & Literature, and German
              Translation & Interpretation
            </ol>
            <ol>
              Applicants must have completed 3 semesters of college or
              university level German or have achieved an equivalent level of
              language proficiency
            </ol>
          </ul>
          <h6 style={{marginLeft:"25px"}}>Terms of Award</h6>
          <ul>
            <ol>
              Scholarship: €2,300; includes tuition, accommodation and a cash
              allowance, health insurance, travel costs
            </ol>
          </ul>
        </p>
        <h4>PhD and Research</h4>
        <p style={{ textAlign: "left" }}>
          <ul>
            <ol>Why you should consider studying for your PhD in Germany?</ol>
            <ol>
              Globally ranked institutions – Germany is home to more
              globally-ranked institutions than any other country outside the
              USA and UK – and eight of them feature in the current top 100
            </ol>
            <ol>
              Dedicated research institutes – In addition to its universities,
              Germany is also home to networks of dedicated research centers,
              including its prestigious Max Planck Institutes
            </ol>
            <ol>
              Affordability – Most German universities charge no tuition fees
              for PhD students, regardless of nationality
            </ol>
            <ol>
              The home of the PhD – The PhD (in its modern form as a
              thesis-based research degree) was actually developed in Germany –
              perhaps this ‘original contribution to knowledge’ can help inspire
              yours?
            </ol>
          </ul>
        </p>
        <h4>Scholarships</h4>
        <p style={{textAlign:"left"}}>
          <ul>
            <ol>
              Student grant – your supervisor may nominate you for a grant of
              €1,250 per month from the German National Academic Foundation
              (Studienstiftung des Deutsche Volkes)
            </ol>
            <ol>
              DAAD scholarships - the German Academic Exchange Service
              (Deutscher Akademischer Austausch Dienst)offers scholarships of
              €1,200 per month for foreign students to study a PhD in Germany
            </ol>
            <ol>
              StipendiumPlus – provided by a group of 13 organizations that
              offer to fund of €1,350 per month to international students in
              Germany
            </ol>
            <ol>
              DFG Funding- German Prof. has already funding available Structured
              doctoral degree program from varies companies
            </ol>
            <ol>
                <ul><ol>
                Max Planck Research Schools - Individual Doctoral Projects
                    </ol>
                    <ol>
                    Fraunhofer Institutes - Individual Doctoral Projects
                    </ol>
                    <ol>
                    Helmholtz Research Schools and Graduate Schools - Individual Doctoral Projects

                    </ol>
                    </ul>
            
            </ol>
          </ul>
        </p>
      </div>
    );
  }
}

export default Scholarships;
