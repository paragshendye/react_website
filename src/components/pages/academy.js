import React, { Component } from "react";

class Academy extends Component {
  state = {};
  render() {
    return (
      <div>
          <div><img src="../../RWTHIA.jpg" style={{"width":"50vw","height":"25vw"}}/></div>
          <h4>RWTH International Academy</h4>
        <div style={{ textAlign: "left", maxWidth: "100%" }}>
          <p>
            The RWTH International Academy is a subsidiary of the RWTH Aachen
            University, which can draw from the University’s whole scope of
            subjects. It thereby conceives scientifically sound and
            practice-oriented programs that are also in sync with market
            demands. These close ties to labs, research facilities, and testing
            centers make continuing education an exceptional experience.
          </p>
          <p>
            The RWTH International Academy offers an array of further services
            to professionalize and facilitate the activities of the departments,
            chairs, and institutes of the RWTH Aachen University, as well as the
            clinics of the University Hospital, in imparting knowledge.
          </p>
        </div>
      </div>
    );
  }
}

export default Academy;
