import React from "react";
import logo from "./logo.svg";
import "./App.css";
import NavBar from "./components/NavBar";
import DemoCarousel from "./components/carousel_web";
import Footer from "./components/Footer";
import { Switch, Route, NavLink } from "react-router-dom";
import Team from "./components/pages/team";
import About from "./components/pages/about";
import Home from "./components/pages/why";
import App from "./App";
import EnglishCourses from "./components/pages/english_courses";
import IamRwth from "./components/pages/iam";
import University from "./components/pages/university";
import Academy from "./components/pages/academy";
import GermanyCounseling from "./components/pages/germany_courses";
import Scholarships from "./components/pages/scholarships";
import Research from "./components/pages/research";


function Myroutes() {
  return (
    <div className="App">
      <div className="header" style={{"position":"-webkit-sticky","position":"sticky","top":"0","zIndex":"999","backgroundColor":"ghostwhite"}}>
      <div className="upper"></div>

      <div>
        <a href="/">
        <img
          src="COMPANY_LOGO.png"
          style={{ "max-width": "70%", height: "12vh", marginTop: "10px" }}
          alt="logo"
         


        />
        </a>
      </div>
      <div id="navbar">
        
        <NavBar />
        </div>
      </div>

      <Route exact path="/whyGermany" component={Home} />
      <Route exact path="/Counseling" component={EnglishCourses} />
      <Route exact path="/IAM" component={IamRwth}/>
      <Route exact path="/" component={App} />
      <Route exact path="/RWTHAachenUniversity" component={University}/>
      <Route exact path="/RWTHAcademy" component={Academy}/>
      <Route exact path = "/germanycounseling" component={GermanyCounseling}/>
      <Route exact path="/Scholarships" component={Scholarships}/>
      <Route exact path="/JournalPublication" component={Research}/>
      <div>
       <Footer/>
      </div>
      </div>
    
  );
}

export default Myroutes;
