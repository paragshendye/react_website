import React from "react";
import logo from "./logo.svg";
import "./App.css";
import NavBar from "./components/NavBar";
import DemoCarousel from "./components/carousel_web";
import Footer from "./components/Footer";
import { Switch, Route, NavLink } from "react-router-dom";
import Team from "./components/pages/team";
import About from "./components/pages/about";
import Home from "./components/pages/why";
import { Link } from "react-scroll";




function App() {
  
  
  return (
 
    <div className="App">
      <DemoCarousel style={{ "max-width": "100%" }} />

      <About />
      <div
      
        style={{
          padding: "5px",
          backgroundColor: "#006baf",
          height: "60px",
          color: "white",
          textAlign: "center",
          
        }}
      >
        <h4 className="scrolledto" style={{"fontSize":"40px"}}>About Us</h4>
      </div>

      
      <Team/>
      <div style={{ height: "200px" }}></div>
      
    </div>
  );
}

export default App;
